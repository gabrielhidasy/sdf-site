#!/bin/bash
mkdir ../html/texts
./generatetexts.sh
./generateblogposts.sh
for i in ../texts/*.txt 
do
	#echo ../html/$(echo $i | cut -d "/" -f 3 | cut -d "." -f 1).html 
	echo $(echo $i | cut -d "/" -f 3 | cut -d "." -f 1)
	python generatepages.py ../html/$(echo $i | cut -d "/" -f 3 | cut -d "." -f 1).html $(echo $i | cut -d "/" -f 3 | cut -d "." -f 1)
done
#python generatepages.py ../html/TO-DO.html TO-DO
#python generatepages.py ../html/contact.html contact
#python generatepages.py ../html/interests.html interests
#python generatepages.py ../html/codes.html codes
#python generatepages.py ../html/blog.html blog
mv ../html/about.html ../html/index.html
rm -r ../html/texts
python mc511_gen.py
