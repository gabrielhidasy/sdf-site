#!/bin/python
import os
a = open("../html/mc511solved.html","w");
a.write("<!DOCTYPE html>");
a.write("<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" /><title>Gabriel Hidasy' page</title><link rel=\"stylesheet\" type=\"text/css\" href=\"files/style.css\" /></head>");
a.write("<body style=\"background-color: #bababa\"><div class=\"container\"><div class=\"title\">Gabriel Hidasy</div><hr />");
a.write("<script type=\"text/javascript\" src=\"files/menu.js\"> </script><hr /><div class=\"content\"><div id=\"title\">Programming Challanges</div>");
a.write("<script type=\"text/javascript\" src=\"texts/mc511.js\"></script>");
itens = os.listdir("../html/files/mc511");
itens.sort();
for i in itens:
	if i[-4:] == "atit":
	 name = open("../html/files/mc511/"+i,"r");
	 a.write("<br>" + name.read() + ": ");
	if i[-3:] == "pdf":
	 a.write(" PDF: <a href=\"files/mc511/" + i + "\">" + i + "</a>");
	if i[-3:] == "cpp":
	 a.write(" Code: ");
	 a.write("<a href=\"files/mc511/"+i+"\">"+i+"</a>");
a.write("</div></div></body></html>");
a.close();

